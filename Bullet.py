import pygame
from pygame.sprite import Sprite

class Bullet(Sprite):
    def __init__(self, settings, screen, ship):
        super(Bullet, self).__init__()
        self.ship = ship
        self.screen = screen
        self.settings = settings
        self.rect = pygame.Rect(ship.rect.centerx - (settings.bullet_width / 2),
                                ship.rect.top,
                                settings.bullet_width,
                                settings.bullet_height)

    def update(self):
        self.rect.centery -= 1

    def draw(self):
        pygame.draw.rect(self.screen, (0, 0, 0), self.rect)
