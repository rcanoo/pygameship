import pygame


class Target():
    def __init__(self, screen, settings):
        self.screen = screen

        # Load the ship image and get its rect.
        self.image = pygame.image.load('images/target.png')

        # Start each new ship at the bottom center of the screen.
        self.rect = self.image.get_rect()
        self.rect.centerx = screen.get_rect().centerx
        self.rect.top = screen.get_rect().top

        self.movingRight = True
        self.movingLeft = False

        self.alive = True

    def moveRight(self, movement):
        self.movingRight = movement
        self.movingLeft = False


    def moveLeft(self, movement):
        self.movingRight = False
        self.movingLeft= movement


    def update(self):
        if self.alive == False:
            return
        if self.movingRight:
            if self.rect.centerx < self.screen.get_rect().width:
                self.rect.centerx += 1
            else:
                self.moveLeft(True)

        elif self.movingLeft:
            if self.rect.centerx > 0:
                self.rect.centerx -= 1
            else:
                self.moveRight(True)

    def draw(self):
        if self.alive == False:
            return
        self.screen.blit(self.image, self.rect)

    def checkCollision(self, bullet):
        if self.alive == False:
            return False
        colliding_x = self.rect.left < bullet.rect.left and self.rect.right > bullet.rect.right
        colliding_y = self.rect.bottom > bullet.rect.top
        if colliding_x and colliding_y:
            self.alive = False
            return True
        return False
